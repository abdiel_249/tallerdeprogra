# Metodos y Tecnicas de Programacion
_Este repositorio servira para subir periodicamente los avances del Proyecto de esta materia_

## Herramientas de Desarrollo

* Java OpenJDK 1.8.0_181
* NetBeans 8.2

## Datos del Ordenador
_Detalles de la computadora empleada para realizar este proyecto_

* Sistema Operativo:  ManjaroLinux Deepen 17.1.12
* Interfaz Grafica: Deepen 15.7 Desktop
* Procesador:  AMD-A9-9420 | 3.0 GHz
* Memoria RAM:  4GB DDR4 + 2GB SWAP
