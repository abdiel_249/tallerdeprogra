/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serealizar;

import java.io.File;
import java.util.ArrayList;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
/**
 *
 * @author jbo
 */
public class main {
    
    @SuppressWarnings({ "unchecked" })
    public static void main(String[] arg) throws FileNotFoundException, IOException, ClassNotFoundException {

        ArrayList <Persona> arrayList1 = new ArrayList <Persona>();
        ArrayList <Persona> arrayList2;

        System.out.println("Datos que vamos a escribir en el fichero:");
        for(int i = 0; i < 10; i++) {
            Persona p = new Persona("niña", 20+i, "la", 2018+i+"");
            arrayList1.add(p);
            System.out.println( "arrayList1[" + i + "] = " + arrayList1.get(i) );
        }
        
        System.out.println("Verificando si el archivo esta vacio");
        
        File archivo = new File("personas.txt");
        if (archivo.length() ==0 ){
            System.out.println("El archivo esta vacio");
        }else{
            System.out.println("El archivo NO esta vacio");
        }
        
        
        System.out.println("Guardando los datos");
        
        FileOutputStream out = new FileOutputStream("personas.txt");
        try (ObjectOutputStream obj = new ObjectOutputStream(out)) {
            obj.writeObject(arrayList1);
        }
        
        System.out.println("Verificando si el archivo esta vacio");
        
        //archivo = new File("personas.txt");
        if (archivo.length() ==0 ){
            System.out.println("El archivo esta vacio");
        }else{
            System.out.println("El archivo NO esta vacio");
        }
        
        System.out.println("Cargando los datos");
        
        
        FileInputStream input = new FileInputStream("personas.txt");
        ObjectInputStream obInput = new ObjectInputStream(input);
        arrayList2 = (ArrayList<Persona>)obInput.readObject();
        System.out.println("");
        System.out.println("Mostrando los datos \n");
        for (Persona p : arrayList2) System.out.println(p.toString());
        
    }
}
