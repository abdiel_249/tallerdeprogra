/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serealizar;

import java.io.Serializable;

/**
 *
 * @author jbo
 */
public class Persona  implements Serializable{
    private String Nombre;
    private int edad;
    private String apellido;
    private String fecha;

    public Persona(String Nombre, int edad, String apellido, String fecha) {
        this.Nombre = Nombre;
        this.edad = edad;
        this.apellido = apellido;
        this.fecha = fecha;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Persona{" + "Nombre=" + Nombre + ", edad=" + edad + ", apellido=" + apellido + ", fecha=" + fecha + '}';
    }
    
}
