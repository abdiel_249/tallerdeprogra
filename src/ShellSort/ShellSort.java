package ShellSort;

public class ShellSort {
    
    
    /**
     * @ShellSort metodo de ordenamiento Shell Sort
     * @param array para ser ordenado
     * @return an array sorted
     */
    public int[] shellSort(int[] array){
        int tam = array.length;
        long initTime, totalTime;
        initTime = System.nanoTime();
        int[] respuesta = shellSort(array, 0, tam/2, tam);
        totalTime = System.nanoTime() - initTime;
        System.out.println("Tiempo trancurrido para un arreglo de tamanio "+array.length+" => "+totalTime);
        return respuesta;
    }
    private int[] shellSort(int[]mat,int pos, int k, int tam){
        if(k>1){
            while(pos+k <mat.length){
                if(mat[pos] > mat[pos+k]){
                    swap(mat, pos, pos+k);
                }
                pos++;
            }
            shellSort(mat, 0, k/2, tam);
        }else   sortInOne(mat);
        return mat;
    }
    private void sortInOne(int[]array){
        int pos=0, decremento;
        while(pos < (array.length-1)){
            if(array[pos] > array[pos+1]){
                swap(array, pos, pos +1);
                decremento = pos;
                while(decremento>=1){
                    if(array[decremento] < array[decremento-1]){
                        swap(array, decremento, decremento-1);
                    }
                    decremento--;
                }
            }
            pos++;
        }
    }
    private void swap(int[]array,int a, int b){
        int aux = array[a];
        array[a] = array[b];
        array[b] = aux;
    }
    
    /**
     * Este metodo devuelve una cadena de un arreglo
     * @param array int[] array
     * @return String => {1, 2, 3, ..., n, }
     */
    public String arrayToString(int[]array){
        String resp = "{";
        for(int i=0;i<array.length; i++){
            resp += (array[i]+", ");
        }
        resp +="}";
        return resp;
    }

    /**
     * Devuelve un arreglo garantizado con datos Repetidos
     * @param tam que sera el tamanio del array que deseamos generar
     * @return Un Array de tamanio "tam"
     */
    public int[] doArrayRepeated(int tam){
        int[] resp = null;
        while(true){
            resp = genArrayRandom(tam);
            if(thereRepeated(resp))
                break;
        }
        return resp;
    }
    private int[] genArrayRandom(int tam){
        int[] resp = new int[tam];
        for(int i=0; i<tam; i++){
            resp[i] = (int) getRandom(tam);
        }
        return resp;
    }
    private boolean thereRepeated(int[] array){
        boolean resp = false;
        for(int i=0; i<array.length-1 && !resp; i++){
            for(int j=i+1; j<array.length && !resp; j++){
                if(array[i] == array[j]){
                    resp = true;
                    break;
                }
            }
            if(resp)
                break;
        }
        return resp;
    }
    
    /**
     * Genera un array con datos NO repetidos y 1 <= valores <= tam
     * @param tam => Tamanio del array que requerimos
     * @return array coo datos No repetidos
     */
    public int[] genArrayNoRep(int tam){
        int[] resp = new int[tam];
        for(int i=0; i<tam; i++){
            while(true){
                int value = (int)getRandom(tam);
                if(!isIn(resp, value, i+1)){
                    resp[i]= value;
                    break;
                }
            }
        }
        return resp;
    }
    // Genera un dato N random talque 1 <= N <= tam
    private double getRandom(int tam){
        double resp = Math.random()*((tam*10)-1)+1;
        return resp;
    }
    /**
     * Responde a la pregunta si un dato esta o no en un array 
     * @param array en el cual se buscaran los datos
     * @param dato el dato que queremos buscar
     * @param pos la posicion maxima en la cual buscaremos
     * @return TRUE si "Dato" fue encontrado FALSE de lo contrario
     */
    private boolean isIn(int[] array, int dato, int pos){
        boolean resp = false; int i=0;
        while(i<pos && !resp){
            if(array[i]==dato)
                resp = true;
            i++;
        }
        return resp;
    }
}
