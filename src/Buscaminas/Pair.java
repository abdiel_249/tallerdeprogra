/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Buscaminas;

/**
 * Taller de Progra->Grupo: 3A
 * @author jbo
 */
public class Pair {
    
    private final int I;
    private final int J;
    
    public Pair(int i, int j){
        this.I = i;
        this.J = j;
    }
    
    public int getI(){
        return this.I;
    }
    
    public int getJ(){
        return this.J;
    }
    public boolean equals(Pair p){
        return p.getI()==this.I && p.getJ() == this.J;
    }
    
}
