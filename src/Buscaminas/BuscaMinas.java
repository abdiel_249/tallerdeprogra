
package Buscaminas;

import java.util.ArrayList;

/**
 * @author jbo -> Abdiel Orellana
 * @career Ing. Informatica
 * @grupo -> 3A individual
 */

public class BuscaMinas {
   /**
    * @Mines -> *
    * @Boxes -> _
    * @EmptyBoxes -> /
    * @Mark -> @
    * @NumberMinesAround -> N+
    */ 
    private final String [][] Board;
    private final int Size;
    private final int NumMines;
    private final ArrayList<Pair> Mines;
    private  boolean ItsAlive;
    
    /*
    * Constructors
    */
    public BuscaMinas (int size){
        this.Size = size; 
        this.Board = new String[Size][Size];
        this.NumMines = Size+4;
        this.Mines = new ArrayList<>(NumMines);
        this.ItsAlive = true;
    }
    public BuscaMinas(int rows, int column){
        Board = new String[rows][column];
        Size = rows;
        NumMines = rows+4;
        Mines = new ArrayList<>(rows+4);
        this.ItsAlive = true;
    }
    public BuscaMinas(int rows, int column, int mines){
        Board = new String[rows][column];
        Size = rows;
        NumMines = mines;
        Mines = new ArrayList<>(mines);
        this.ItsAlive = true;
    }
    
    // call this method when you want to start the game
    public void initGame(){
        fillBoard();
        putMines(NumMines);
        showBoard();
    }
    
    // method that performs the actions of the playing game
    public void play(int i, int j){
        if(itsIn(new Pair(i,j))){
            fillAllMines();
            showBoard();
            this.ItsAlive = false;
        }else{
            int num = lookAround(i, j);
            if(num==0){
                searchEmptyBoxes(i, j);
            }else
                Board[i][j]=num+"";
            showBoard();
        }
    }
    
    // this method look around of a box and to return a Integer whit the number of mines around
    public int lookAround(int x, int y){
        int resp=0;
        for(int i=x-1; i<=x+1; i++){
            for(int j=y-1; j<=y+1; j++){
                if((itsValid(i, j))){
                    if(itsIn(new Pair(i,j))){
                        resp++; 
                   }
                }
            }
        }
        return resp;
    }
    
    // fill the board whit String ->"_"
    private void fillBoard(){
        for(int i=0; i<Board.length; i++){
            for(int j=0; j<Board[i].length; j++){
                Board[i][j] = "_";
            }
        }
    }
    
    // show a board ising a "sms" method to print    
    public void showBoard(){
        for(int i=0; i<Board.length; i++){
            for(int j=0; j<Board[i].length; j++){
                sms(Board[i][j] +"\t");
            }
            sms("\n");
        }
    }
    
    // put the ArrayList to Mines
    private void putMines(int num){
        while(num>0){
            Pair aux = getPair();// -> call the method "getPair()"
            if(!itsIn(aux)){
                Mines.add(aux);
                num--;
            }        
        }
    }
    
    // return a Pair with a random datas for the mines
    private Pair getPair(){
        int i=getNumValit();
        int j=getNumValit();
        return new Pair(i,j);
    }
    private int getNumValit(){
        int resp;
        while(true){
                resp = (int)getNumRandom();
            if(resp<Size && resp >=0)
                break;
        }
        return resp;
    }
    
    // fill a Board with all mines in the ArrayList of Mines
    private void fillAllMines(){
        int i,j;
        for(Pair p : Mines){
            i=p.getI(); j=p.getJ();
            Board[i][j] = "*";
        }
    }
    
    // Search a Empty Boxes around of one position "i, j" using a form of BackTracking
    public void searchEmptyBoxes(int i, int j){
        if(itsValid(i, j)){
            int num = lookAround(i, j);
            if(num==0){
                if(Board[i][j].equals("_")){
                    Board[i][j]="/";
                    searchEmptyBoxes(i, j-1);
                    searchEmptyBoxes(i-1, j-1);
                    searchEmptyBoxes(i-1, j);
                    searchEmptyBoxes(i-1, j+1);
                    searchEmptyBoxes(i, j+1);
                    searchEmptyBoxes(i+1, j+1);
                    searchEmptyBoxes(i+1, j);
                    searchEmptyBoxes(i+1, j-1);
                }
            }else
                Board[i][j]=num+"";
        }
    }
    
    // return a boolean response, true if the Pair input it's in the Mines Array
    private boolean itsIn(Pair p){
        boolean resp = false;
        for(int i=0; i<Mines.size(); i++){
            Pair aux = Mines.get(i);
            if(p.equals(aux)){
                resp = true;
                break;
            }
        }
        return resp;
    }
    private void sms(String cad){
        System.out.print(cad);
    }
    public boolean itsAlive(){
        return this.ItsAlive;
    }
    private boolean itsValid(int i, int j){
        return i>=0 && i<Board.length && j>=0 && j<Board[i].length;
    }
    private double getNumRandom(){
        double resp = Math.random()*(Size-1);
        return resp;
    }
    
    
}
