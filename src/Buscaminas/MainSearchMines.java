/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Buscaminas;

import java.util.Scanner;

/**
 *
 * @author jbo
 */
public class MainSearchMines {
    public static void main(String[]args){
        Scanner in = new Scanner(System.in);
        int tam = in.nextInt();
        BuscaMinas b = new BuscaMinas(tam);
        b.initGame();
        while(b.itsAlive()){
            int i = in.nextInt();
            int j = in.nextInt();
            b.play(i, j);
        }
    }
}
