/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author jbo
 */
public class ProfileSaved implements Serializable{
    
    private static Board Board;
    private static String Name; 
    private static int Time;
    
    public ProfileSaved(Board b, String name){
        this.Board = b;
        this.Name = name;
        
    }
    public ProfileSaved(String name, int time){
        this.Name = name;
        this.Time = time;
    }
    public String getName() {   return Name;    }
    public int getTime()    {   return Time;    }
    
    public static void saveGame(Board b, String name) 
            throws  IOException,
                    FileNotFoundException,
                    ClassNotFoundException{
        
        ProfileSaved ps = new ProfileSaved(b, name);
        SaveGame.saveGame(ps);
    }
    
    public static void saveScore(String name, int time) 
            throws  IOException, 
                    FileNotFoundException, 
                    ClassNotFoundException{
        
        ProfileSaved ps = new ProfileSaved(name, time);
        SaveGame.saveScore(ps);
    }
    
}
