/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author jbo
 */
public class SaveGame {
    
    private static ArrayList<ProfileSaved> gameSaved;
    private static ArrayList<ProfileSaved> scoreSaved;
    
    private static String routGameSaved = "datas/gamesSaved.txt";
    private static String routScoreSaved = "datas/scoreSaved";
    
    @SuppressWarnings({ "unchecked" })
    public static void saveGame(ProfileSaved ps)    //  cat 1 
            throws  FileNotFoundException, 
                    IOException, 
                    ClassNotFoundException{   
        
        gameSaved = new ArrayList<>();
        
        if(fileIsEmpy(1)){  // if the file its empy  only save datas
            sms("EL archivo esta vacio procediendo a guardar");
        FileOutputStream out = new FileOutputStream(routGameSaved);
            try (ObjectOutputStream obj = new ObjectOutputStream(out)) {
                obj.writeObject(gameSaved);
            }
            
        }else{  //  else frist upload de array list with datas then save all news datas
            // load Datas
            sms("EL archivo NO esta vacio procediendo a Cargar y Guardar");
            
            FileInputStream input = new FileInputStream(routGameSaved);
            ObjectInputStream obInput = new ObjectInputStream(input);
            gameSaved = (ArrayList<ProfileSaved>)obInput.readObject();
            // Save all datas
            FileOutputStream out = new FileOutputStream(routGameSaved);
            try (ObjectOutputStream obj = new ObjectOutputStream(out)) {
                obj.writeObject(gameSaved);
                sms("Datos guardados exitosamente: "+gameSaved.size());
            }
        }   
    }
    
    @SuppressWarnings({ "unchecked" })
    public static void saveScore(ProfileSaved ps)   // cat 2
            throws  FileNotFoundException, 
                    IOException, 
                    ClassNotFoundException{  
        
        scoreSaved = new ArrayList<>();
        
        if(fileIsEmpy(2)){  // if the file its empy  only save datas
            scoreSaved.add(ps);
            sms("EL archivo esta vacio procediendo a guardar");
            FileOutputStream out = new FileOutputStream(routScoreSaved);
            try (ObjectOutputStream obj = new ObjectOutputStream(out)) {
                obj.writeObject(scoreSaved);
            }
            
        }else{  //  else frist upload de array list with datas then save all news datas
            // load Datas
            sms("EL archivo NO esta vacio procediendo a Cargar y Guardar");
            FileInputStream input = new FileInputStream(routScoreSaved);
            ObjectInputStream obInput = new ObjectInputStream(input);
            scoreSaved = (ArrayList<ProfileSaved>)obInput.readObject();
            
            // Save all datas
            scoreSaved.add(ps);
                
            FileOutputStream out = new FileOutputStream(routScoreSaved);
            try (ObjectOutputStream obj = new ObjectOutputStream(out)) {
                obj.writeObject(scoreSaved);
                sms("Datos guardados exitosamente: "+scoreSaved.size());
            }
        }
        
    }
    
    private static boolean fileIsEmpy(int cat){
        boolean resp = false;   File file;
        if(cat == 1){
            file = new File(routGameSaved);
            if(file.length()==0)    resp = true;
        }else{
            file = new File(routScoreSaved);
            if(file.length()==0)    resp = true;
        }
        return resp;
    }
    
    private static void sme(String cad){
        System.err.println(cad);
    }
    
    private static void sms(String cad){
        System.out.println(cad);
    }
}

