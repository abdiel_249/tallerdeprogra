/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jbo
 */
public class Board extends javax.swing.JFrame {

    protected static Casilla[][] casillas;
    protected static int numMinas, tamanio,    time;
    protected static int numBanderas;
    protected static boolean itsFristPlaying, isAlive;
    protected static ArrayList<Pair> Mines;
    protected static Timer timer;
    protected static TimerTask job;
    protected static int numCasillas;
    
    public Board() {
        initComponents();
        loadComponents();
        loadMatrix();
        updateBoardGamePanel();
    }
    
    public void loadComponents(){
        this.tamanio = MainMenu.tamanio;
        this.numMinas = MainMenu.numMinas;
        this.numBanderas = numMinas; 
        this.time = 0;
        this.numCasillas=tamanio*tamanio;
        this.itsFristPlaying = false;
        this.isAlive = true;
        this.casillas = new Casilla[tamanio][tamanio];
        this.Mines = new ArrayList<>();
        this.boardGamePanel.setMinimumSize(new Dimension(400,400));
    }
    
    public static void jugar(Casilla c){
        int val;
        if(isAlive && numCasillas >0 ){
            if(!itsFristPlaying) {
                Mines.add(new Pair(c.getI(), c.getJ()));
                loadMines();
                Mines.remove(0);
                loadMinesInBoard();
                val = lookAround(c);
                reloj();
                
                    c.setText(val+"");
                
                itsFristPlaying = true;
            }else if(c.isMine()){
                showAllMinesInBoard();
                timer.cancel();
                c.pressed();
                //isAlive = false;
            }else{
                val = lookAround(c);
                
                    c.setText(val+"");
            
            }
            reduceNumBoxes();
        }else if (numCasillas==0){
            JOptionPane.showMessageDialog(null,"Se acabo el juego Bart :v");
            sms("El Juego Termino");
        }
    }
    public static void searchEmptyBoxes(int i, int j){
        try{
            if(itsValid(i, j)){
                int num = lookAround(casillas[i][j]);
                if(num==0){
                    if(casillas[i][j].isEnabled()){
                        casillas[i][j].setText("_");
                        searchEmptyBoxes(i, j-1);
                        searchEmptyBoxes(i-1, j-1);
                        searchEmptyBoxes(i-1, j);
                        searchEmptyBoxes(i-1, j+1);
                        searchEmptyBoxes(i, j+1);
                        searchEmptyBoxes(i+1, j+1);
                        searchEmptyBoxes(i+1, j);
                        searchEmptyBoxes(i+1, j-1);
                    }
                }else
                    casillas[i][j].setText(num+"");
            }
        }catch(StackOverflowError error){
            System.err.println(" x Error de tipo "+error.getMessage());
        }catch(Exception e){
            sms("Se a producido un error inesperado >:o");
        }
    }
    public static int lookAround(Casilla c){
        int resp=0, x = c.getI(), y = c.getJ();
        for(int i=c.getI()-1; i<=c.getI()+1; i++){
            for(int j=c.getJ()-1; j<=c.getJ()+1; j++){
                if((itsValid(i,j))){
                    if(itsIn(new Pair(i,j))){
                        resp++; 
                   }
                }
            }
        }
        return resp;
    }
    public static boolean itsValid(int i, int j){
        boolean resp =  i<tamanio && i>=0 && j<tamanio && j>=0 
                        && casillas[i][j].isVisible();
        return resp;
    }
    
    // all this methos are be load mines ArrayList
    public static void loadMines(){
        int num = numMinas;
        while(num>0){
            Pair aux = getPair();// -> call the method "getPair()"
            if(!itsIn(aux)){
                Mines.add(aux);
                sms("Se aniadio mina en casilla "+aux.toString());
                num--;
            }        
        }
    }
    private static Pair getPair(){
        Pair aux; int i, j;
        while(true){
            i= getNumValit();
            j = getNumValit();
            if(itsValid(i, j)){
                aux = new Pair(i,j);
                break;
            }
        }
        return aux;
    }
    private static int getNumValit(){
        int resp;
        while(true){
                resp = (int)getNumRandom();
            if(resp<tamanio && resp >=0)
                break;
        }
        return resp;
    }
    private static boolean itsIn(Pair p){
        boolean resp = false;
        for(int i=0; i<Mines.size(); i++){
            Pair aux = Mines.get(i);
            if(p.equals(aux)){
                resp = true;
                break;
            }
        }
        return resp;
    }
    private static double getNumRandom(){
        double resp = Math.random()*(tamanio-1);
        return resp;
    }   //  end methos to load all Mines ArrayList
    
    public void updateBoardGamePanel(){
        boardGamePanel.setLayout(new java.awt.GridLayout(tamanio, tamanio));
        boardGamePanel.updateUI();
        loadBandera();
    }
    
    public void loadMatrix(){
        for(int i=0; i<tamanio; i++){
            for(int j=0; j<tamanio; j++){
                this.casillas[i][j] = new Casilla(i, j, 1);
                this.boardGamePanel.add(this.casillas[i][j]);
                sms("Se aniadio una casilla en "+casillas[i][j].toString());
            }
        }
    }
    public static void loadMinesInBoard(){
        for(Pair p: Mines){
            casillas[p.getI()][p.getJ()].setType(2);
        }
    }
    public static void showAllMinesInBoard(){
        for(Pair p: Mines){
            casillas[p.getI()][p.getJ()].setText("M");
            casillas[p.getI()][p.getJ()].pressed();
            reduceNumBoxes();
        }
    }
    
    public static void reloj(){
        time = 1;
        timer = new Timer();
        job = new TimerTask() {
            @Override
            public void run() {
                timerJTextF.setText(time+"");
                time++;
            }
        };
        timer.schedule(job,0,1000);
    }
    public static void loadBandera(){
        numBanderasJTF.setText(numBanderas+"");
    }
    public static void reduceNumBoxes(){
        numCasillas--;
        sms("Numero de casillas restantes: "+numCasillas);
    }
    public static void addNumBoxes(){
        numCasillas++;
        sms("Numero de casillas restantes: "+numCasillas);
    }
    
    public static void marcarBandera(){     numBanderas--;  loadBandera();  }
    public static void desmarcarBandera(){  numBanderas++;  loadBandera();  }
    
    public void mostrar()           {   this.setVisible(true);  }
    public static void sms(String cad)    {   System.out.println(cad);    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barPanel = new javax.swing.JPanel();
        backButton = new javax.swing.JButton();
        timerJTextF = new javax.swing.JTextField();
        numBanderasJTF = new javax.swing.JTextField();
        saveGameButton = new javax.swing.JButton();
        boardGamePanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        barPanel.setBackground(new java.awt.Color(89, 0, 36));

        backButton.setFont(new java.awt.Font("TSCu_Comic", 3, 14)); // NOI18N
        backButton.setText("Atras");

        timerJTextF.setEditable(false);

        numBanderasJTF.setEditable(false);

        saveGameButton.setFont(new java.awt.Font("TSCu_Comic", 3, 14)); // NOI18N
        saveGameButton.setText("Guardar");
        saveGameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveGameButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout barPanelLayout = new javax.swing.GroupLayout(barPanel);
        barPanel.setLayout(barPanelLayout);
        barPanelLayout.setHorizontalGroup(
            barPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(barPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 136, Short.MAX_VALUE)
                .addComponent(timerJTextF, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(numBanderasJTF, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                .addComponent(saveGameButton)
                .addGap(15, 15, 15))
        );
        barPanelLayout.setVerticalGroup(
            barPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(barPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(barPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backButton)
                    .addComponent(timerJTextF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numBanderasJTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveGameButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        boardGamePanel.setBackground(new java.awt.Color(0, 134, 143));
        boardGamePanel.setLayout(new java.awt.GridLayout(8, 10));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(barPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(boardGamePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(barPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(boardGamePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveGameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveGameButtonActionPerformed
        // TODO add your handling code here:
        String name = JOptionPane.showInputDialog(this,"Cual es tu nombre?");
        sms("El nombre del Dog es: "+name);
        try {
            ProfileSaved.saveGame(this, name);
        } catch (IOException ex) {
            //  Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }//GEN-LAST:event_saveGameButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Board().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JPanel barPanel;
    private javax.swing.JPanel boardGamePanel;
    private static javax.swing.JTextField numBanderasJTF;
    private javax.swing.JButton saveGameButton;
    private static javax.swing.JTextField timerJTextF;
    // End of variables declaration//GEN-END:variables
}
