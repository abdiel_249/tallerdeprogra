/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;

/**
 *
 * @author jbo
 */
public class Pair {
    private int I, J;
    
    //  Constructor
    public Pair(int i, int j){
        this.I = i;
        this.J = j;
    }
    
    //  Default Getters & Setters
    public int getI(){  return  this.I;  }
    public int getJ(){  return  this.J;  }
    public void setI(int i){    this.I = i;    }
    public void setJ(int j){    this.J = j;    }
    
    public Pair getPair(){  return this;    }
    
    public boolean equals(Pair p){
        return this.I == p.getI() && this.J == p.getJ();
    }
    public String toString(){
        return "["+I+"]["+J+"]";
    }
}
