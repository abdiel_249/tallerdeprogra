/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

/**
 *
 * @author jbo
 */
public class Casilla extends JButton{
    
    private int I, J, Type;
    private boolean isPressed, bandera;
    private MouseListener click;
    
    public Casilla(int i, int j, int tipo){
        this.I = i;
        this.J = j;
        this.Type = tipo;   // 1->normal | 2->Mine
        this.isPressed = false;
        this.bandera = false;
        
        this.setBackground(Color.ORANGE);
        this.setPreferredSize(new Dimension(35,35));
        
        this.click = new MouseAdapter() {
            public void mousePressed(MouseEvent me){
                int modificador =  me.getModifiers();
                if((modificador & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK){
                    //  left button pressed
                    clickIzquierdo();
                }else if((modificador & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK){
                    // right button pressed
                    clickDerecho();
                }
            }
        };
        this.addMouseListener(click);
    }
    
    public void clickIzquierdo(){
        if(!isPressed){
            Board.jugar(this);
            //this.setText(this.Type+"");
            isPressed = true;
        }
        sms("Se hizo click izquierdo en la casilla "+ this.toString());
    }
    public void clickDerecho(){
        if(!isPressed){
            if(!bandera) {
                this.setEnabled(false);
                Board.marcarBandera();
                Board.reduceNumBoxes();
            }else {
                this.setEnabled(true);
                Board.desmarcarBandera();
                Board.addNumBoxes();
            }
            bandera = !bandera;
        }
        sms("Se hizo click derecho en la casilla "+ this.toString());
    }
    
    //  Default Getters & Setters
    public int getI(){  return  this.I;  }
    public int getJ(){  return  this.J;  }
    public int getType(){  return  this.Type;  }
    public void setI(int i){    this.I = i;    }
    public void setJ(int j){    this.J = j;    }
    public void setType(int type){    this.Type = type;    }
    public boolean isMine(){   return this.Type == 2;  }
    
    private void sms(String cat){   System.out.println(cat);    }
    public void pressed(){
        this.isPressed = true;
    }
    @Override
    public String toString(){   return "Casilla ["+I+"]["+J+"]";    }
    
}
