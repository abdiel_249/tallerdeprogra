/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;

/**
 *
 * @author jbo
 */
public class CruzBoard extends Board{
    
    
    public CruzBoard(){
        super.setTitle("Buscaminas en Cruz");
        makeCross();
    }
    
    public void makeCross(){
        int min = tamanio/3, max = (tamanio/3)*2;
        for (int i = 0; i < tamanio; i++) {
            for (int j = 0; j < tamanio; j++) {
                if((i<min || i>=max) && (j<min || j>=max)){
                    casillas[i][j].setVisible(false);
                }
            }
        }
    }
    
}
