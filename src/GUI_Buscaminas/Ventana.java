/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author jbo
 */
public class Ventana extends JFrame{
    
    private static ArrayList<ProfileSaved> gameSaved;
    private static ArrayList<ProfileSaved> scoreSaved;
    
    private static String routGameSaved = "datas/gamesSaved.txt";
    private static String routScoreSaved = "datas/scoreSaved";
    
    private JPanel mainPanel;
    
    public Ventana()throws IOException, FileNotFoundException, ClassNotFoundException{
    
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle("Juegos Guardados");
        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        this.mainPanel = new JPanel();
        this.setSize(400, 400);
        //this.mainPanel.setSize(new Dimension(400, 400));
        mainPanel.setBackground(Color.GREEN);
        this.add(mainPanel);
//        initComponents();
        loadGames();
//        pack();
       this.setVisible(true);
        mainPanel.setVisible(true);
        
        this.setLocationRelativeTo(null);
        this.repaint();
    }
    
    public  void loadGames() throws IOException, FileNotFoundException, ClassNotFoundException{
        saveGame();
        for(ProfileSaved ps: gameSaved){
            GameSavedItem gs = new GameSavedItem(ps.getName(), ps);
            gs.setVisible(true);
            mainPanel.add(gs);
            sms("Se aniadio un juego guardado");
        }
    }
    
    
    public void initComponents(){
        
    }
    
    
    public void mostrar(){
        this.setVisible(true);
    }
    
    
    @SuppressWarnings({ "unchecked" })
    public static void saveGame()    //  cat 1 
            
            throws  FileNotFoundException, 
                    IOException, 
                    ClassNotFoundException{   
        
        //gameSaved = new ArrayList<>();
        
        if(! fileIsEmpy(1)){  // if the file don't empy  only save datas
            // load Datas
            sms("EL archivo NO esta vacio procediendo a Cargar y Guardar");
            
            FileInputStream input = new FileInputStream(routGameSaved);
            ObjectInputStream obInput = new ObjectInputStream(input);
            gameSaved = (ArrayList<ProfileSaved>)obInput.readObject();
            sms("Se cargaron los datos: "+gameSaved.size());
        }   
    }
    
    @SuppressWarnings({ "unchecked" })
    public static void saveScore()   // cat 2
            throws  FileNotFoundException, 
                    IOException, 
                    ClassNotFoundException{  
        
        scoreSaved = new ArrayList<>();
        
        if(! fileIsEmpy(2)){  
            
            // load Datas
            sms("EL archivo NO esta vacio procediendo a Cargar y Guardar");
            FileInputStream input = new FileInputStream(routScoreSaved);
            ObjectInputStream obInput = new ObjectInputStream(input);
            scoreSaved = (ArrayList<ProfileSaved>)obInput.readObject();
            sms("datos guardados: "+scoreSaved.size());
        }
        
    }
    
    private static boolean fileIsEmpy(int cat){
        boolean resp = false;   File file;
        if(cat == 1){
            file = new File(routGameSaved);
            if(file.length()==0)    resp = true;
        }else{
            file = new File(routScoreSaved);
            if(file.length()==0)    resp = true;
        }
        return resp;
    }
    
    private static void sme(String cad){
        System.err.println(cad);
    }
    
    private static void sms(String cad){
        System.out.println(cad);
    }
    
}
