/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI_Buscaminas;

/**
 *
 * @author jbo
 */
public class FigureBoard extends Board {
    private int center;
    private int min;
    private int max;
    private int k;
    
    public FigureBoard(){
        super.setTitle("Buscaminas en Figura");
        center = (int)tamanio/2;
        k = (int)(tamanio/4);
        min = center-k;
        max = center+k;
        makeFigure();
    }
    
    public void makeFigure(){
        int i = min, mn = center, mx = center;
        while(i<=max){
            
            if(i==(center+1)){   mn+=2; mx-=2;  }
            
            if(i<=center){
                //sms("mn / mx("+mn+","+mx+")");
                for (int j = mn; j <= mx; j++) {
                    casillas[i][j].setVisible(false);
                    sms("Se borro la casilla "+casillas[i][j].toString());
                }
                mn--; mx++;
            }else{
                //sms("mn / mx("+mn+","+mx+")");
                for (int j = mn; j <= mx; j++) {
                    casillas[i][j].setVisible(false);
                    sms("Se borro la casilla "+casillas[i][j].toString());
                }
                mn++; mx--;
            }
            i++;
        }
        casillas[max][center].setVisible(false);
    }
    
    private boolean isValidForHide(int i, int j){
        boolean resp = (i+j>=6) && (j-i<=2) && (i+j<=10);
        return resp;
    }
}
